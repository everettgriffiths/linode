<?php namespace Hampel\Linode;

use Hampel\Linode\LinodeException;

/**
 * Utilty Linode API group
 * https://www.linode.com/api/utility
 */
class Avail extends LinodeBase
{
	/** @var string Prefix for commands */
	protected $prefix = 'avail';

	/** @var array allowable parameters to create and update calls */
	protected $parameters = array(
	);


	/**
	 * https://www.linode.com/api/utility/avail.datacenters
	 *
	 * @return array of Datacenters
	 */
	public function datacenters()
	{
		$command = $this->prefix . '.datacenters';
		$request_headers = array();
		$request_options = array();

		return $this->linode->get($command, $request_headers, $request_options);
	}

    /**
     * DistributionID - numeric (optional) 
     * Limit the results to StackScripts that can be applied to this DistributionID 
     * DistributionVendor - string (optional) 
     * Debian, Ubuntu, Fedora, etc. 
     * keywords - string (optional) Search terms 
     * 
     * https://www.linode.com/api/utility/avail.stackscripts
     */
    public function stackscripts($DistributionID, $DistributionVendor=null,$keywords=null) {
		$command = $this->prefix . '.stackscripts';
		$request_headers = array();
		$request_options = array();

        $request_options['query']['DistributionID'] = $DistributionID;
        if ($DistributionVendor) {
            $request_options['query']['DistributionVendor'] = $DistributionVendor;
        }
        if ($keywords) {
            $request_options['query']['keywords'] = $keywords;
        }
        
        //if (!array_key_exists('DomainID', $data)) throw new LinodeException("Invalid data returned from {$command} - no DomainID found");

		//return $data['DomainID'];
		
		return $this->linode->get($command, $request_headers, $request_options);
    
    }

    /**
     * avail.linodeplans
     *
     * Returns a structure of Linode PlanIDs containing the Plan label and the availability in each Datacenter. 
     * Please note, this method is deprecated and will be removed in the future.
     *
     * https://www.linode.com/api/utility/avail.linodeplans    
     *
     * @param numeric $PlanID - numeric (optional) 
     * @return array
     */
    public function linodeplans($PlanID=null) {
		$command = $this->prefix . '.linodeplans';
		$request_headers = array();
		$request_options = array();

        if ($PlanID) $request_options['query']['PlanID'] = (int) $PlanID;
		
		return $this->linode->get($command, $request_headers, $request_options);
    
    }
    
    
    
	public function __call($method, $args) {

		$className = get_class($this);

		throw new \BadMethodCallException("Call to undefined method {$className}::{$method}()");
	}
}
