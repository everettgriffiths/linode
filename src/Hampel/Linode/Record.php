<?php namespace Hampel\Linode;

use Hampel\Linode\LinodeException;

/**
 * Domain.Resource Linode API group
 *
 */
class Record extends LinodeBase
{
	/** @var string Prefix for commands */
	protected $prefix = 'domain.resource';

	protected $parameters = array(
		"domainid", // required
		"resourceid", // required for update
		"type", // required one of NS, MX, A, AAAA, CNAME, TXT, SRV
		"name", // default "" - hostname of FQDN. When type=MX the subdomain to delegate to the target MX server
		"target", 	// default ""
		// when type=MX the hostname
		// when type=CNAME the target of the alias
		// when type=TXT the value of the record
		// when type=A or AAAA the token of [remote_addr] will be substituted with the IP address of the request
		"priority", // default 10
		"weight", // default 5,
		"port", // default 80,
		"protocol", // default 'udp' - the protocol to append to an SRV record. Ignored on other record types
		"ttl_sec" // default 0 - TTL. Leave as 0 to accept Linode default
	);

	/**
	 * domain.resource.create
	 *
	 * @param string $domainid	domainid to create a resource for
	 * @param string $type		must be one of "NS, MX, A, AAAA, CNAME, TXT, or SRV"
	 * @param array $options	array of key-value pairs for other optional values (see defaults array)
	 *
	 * @throws LinodeException
	 *
	 * @return number resource identifier
	 */
	public function create($domainid, $type, array $options = array())
	{
		$options['domainid'] = $domainid;
		$options['type'] = $type;

		$command = $this->prefix . '.create';
		$request_headers = array();
		$request_options['query'] = $this->processOptions($options);

		$data = $this->linode->get($command, $request_headers, $request_options);

		if (!array_key_exists('ResourceID', $data)) throw new LinodeException("Invalid data returned from {$command} - no ResourceID found");

		return $data['ResourceID'];
	}

	/**
	 * domain.resource.update
	 *
	 * @param string $domainid		domainid to create a resource for
	 * @param string $resourceid	resourceid to update
	 * @param array $options		array of key-value pairs for other optional values (see defaults array)
	 *
	 * @throws LinodeException
	 *
	 * @return number resource identifier
	 */
	public function update($domainid, $resourceid, array $options = array())
	{
		$options['domainid'] = $domainid;
		$options['resourceid'] = $resourceid;

		$command = $this->prefix . '.update';
		$request_headers = array();
		$request_options['query'] = $this->processOptions($options);

		$data = $this->linode->get($command, $request_headers, $request_options);

		if (!array_key_exists('ResourceID', $data)) throw new LinodeException("Invalid data returned from {$command} - no ResourceID found");

		return $data['ResourceID'];
	}

	/**
	 * domain.resource.delete
	 *
	 * @param number $domain_id		Linode domain ID resource belongs to
	 * @param number $resourceid 	Linode resource ID to delete for this domain
	 *
	 * @throws LinodeException
	 *
	 * @return number resource of the domain deleted
	 */
	public function delete($domainid, $resourceid)
	{
		$command = $this->prefix . '.delete';
		$request_headers = array();
		$request_options['query'] = array(
			"domainid" => intval($domainid),
			"resourceid" => intval($resourceid)
		);

		$data = $this->linode->get($command, $request_headers, $request_options);

		if (!array_key_exists('ResourceID', $data)) throw new LinodeException("Invalid data returned from {$command} - no ResourceID found");

		return $data['ResourceID'];
	}

	public function listRecord($domainid, $resourceid = 0)
	{
		$command = $this->prefix . '.list';
		$request_headers = array();
		$request_options = array();

		$resourceid = intval($resourceid);

		$request_options['query'] = array("domainid" => intval($domainid));
		if ($resourceid > 0)
		{
			$request_options['query']['resourceid'] = $resourceid;
		}

		$response = $this->linode->get($command, $request_headers, $request_options);

		if (!is_array($response) OR empty($response))
		{
			return null;
		}

		if ($resourceid > 0)
		{
			return array_change_key_case($response[0]);
		}

		array_walk($response, function(&$item, $key) {
			$item = array_change_key_case($item);
		});

		return $response;
	}

	public function __call($method, $args)
	{
		$className = get_class($this);

		if ($method == 'list')
		{
			if (!isset($args[0])) throw new InvalidArgumentException("Missing argument 1 in call to {$classname}::{$method}");
			return $this->listRecord($args[0], isset($args[1]) ? $args[1] : 0);
		}

		throw new \BadMethodCallException("Call to undefined method {$className}::{$method}()");
	}
}

?>