<?php namespace Hampel\Linode;

use Hampel\Linode\LinodeException;

/**
 * Domain Linode API group
 *
 */
class Domain extends LinodeBase
{
	/** @var string Prefix for commands */
	protected $prefix = 'domain';

	/** @var array allowable parameters to create and update calls */
	protected $parameters = array(
		"domain", // required - zone's name
		"domainid", // required for update
		"description", // default "" - currently undisplayed
		"type", // required must be master or slave
		"soa_email", // required when type=master
		"refresh_sec", // default 0
		"retry_sec", // default 0
		"expire_sec", // default 0
		"ttl_sec", // default 0
		"status", // default 1 - must be 0, 1 or 2 (disabled, active, edit mode)
		"master_ips", // default "" - when type=slave, the zone's master DNS servers list, semicolon separated
		"axfr_ips" // default "" - IP addresses allowed to AXFR the entire zone, semicolon separated
	);

	/**
	 * domain.create
	 *
	 * @param array $options	array of key-value pairs for other optional values (see parameters array)
	 *
	 * @throws LinodeException
	 *
	 * @return number domain identifier
	 */
	public function create($name, $type = 'master', array $options = array())
	{
		$options['domain'] = $name;
		$options['type'] = $type;

		$command = $this->prefix . '.create';
		$request_headers = array();
		$request_options['query'] = $this->processOptions($options);

		$data = $this->linode->get($command, $request_headers, $request_options);

		if (!array_key_exists('DomainID', $data)) throw new LinodeException("Invalid data returned from {$command} - no DomainID found");

		return $data['DomainID'];
	}

	/**
	 * domain.update
	 *
	 * @param number $domainid	domainid to update
	 * @param array $options	array of key-value pairs for other optional values (see defaults array)
	 *
	 * @throws LinodeException
	 *
	 * @return number domain identifier
	 */
	public function update($domainid, array $options = array())
	{
		$options['domainid'] = $domainid;

		$command = $this->prefix . '.update';
		$request_headers = array();
		$request_options['query'] = $this->processOptions($options);

		$data = $this->linode->get($command, $request_headers, $request_options);

		if (!array_key_exists('DomainID', $data)) throw new LinodeException("Invalid data returned from {$command} - no DomainID found");

		return $data['DomainID'];
	}

	/**
	 * domain.delete
	 *
	 * @param number $domainid	Linode domain ID to delete
	 *
	 * @throws LinodeException
	 *
	 * @return number domainid of the domain deleted
	 */
	public function delete($domainid)
	{
		$command = $this->prefix . '.delete';
		$request_headers = array();
		$request_options['query'] = array("domainid" => intval($domainid));

		$data = $this->linode->get($command, $request_headers, $request_options);

		if (!array_key_exists('DomainID', $data)) throw new LinodeException("Invalid data returned from {$command} - no DomainID found");

		return $data['DomainID'];
	}

	/**
	 * domain.list
	 *
	 * @param number $domainid	optional - if specified, lists details for this domain ID, otherwise lists all available domains
	 *
	 * @return array of DomainData objects or single DomainData object if domainid specified
	 */
	public function listDomain($domainid = 0)
	{
		$command = $this->prefix . '.list';
		$request_headers = array();
		$request_options = array();

		$domainid = intval($domainid);

		if ($domainid > 0)
		{
			$request_options['query'] = array("domainid" => $domainid);
		}

		$response = $this->linode->get($command, $request_headers, $request_options);

		if (!is_array($response) OR empty($response))
		{
			return null;
		}

		if ($domainid > 0)
		{
			return array_change_key_case($response[0]);
		}

		array_walk($response, function(&$item, $key) {
			$item = array_change_key_case($item);
		});

		return $response;
	}

	public function __call($method, $args)
	{
		if ($method == 'list')
		{
			return $this->listDomain(isset($args[0]) ? $args[0] : 0);
		}

		$className = get_class($this);

		throw new \BadMethodCallException("Call to undefined method {$className}::{$method}()");
	}
}

?>