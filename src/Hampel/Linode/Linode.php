<?php namespace Hampel\Linode;

use Hampel\Linode\LinodeException;

/**
 * Domain Linode API group
 *
 */
class Linode extends LinodeBase
{
	/** @var string Prefix for commands */
	protected $prefix = 'linode';

	/** @var array allowable parameters to create and update calls 
	Note: we use all lowercase variants
	*/
	// https://www.linode.com/api/linode/linode.update
	protected $parameters = array(
		'linodeid', // required for update
		'label',
		'lpm_displayGroup', // Alpha and underscore, no spaces
		'alert_cpu_enabled', // boolean
		'alert_cpu_threshold', // percentage 0-800 
		'alert_diskio_enabled', // boolean
		'alert_diskio_threshold', // numeric
		'alert_bwin_enabled', // boolean
		'alert_bwin_threshold', // numeric Mb/sec
		'alert_bwout_enabled', // boolean
		'alert_bwout_threshold', //numeric Mb/sec
		'alert_bwquota_enabled', // bolean
		'alert_bwquota_threshold', // numeric Mb/sec
		'backupWindow', // numeric
		'backupWeeklyDay', // numeric
		'watchdog', // boolean enable Lassie shutdown watchdog
		'ms_ssh_disabled', // boolean
		'ms_ssh_user', // string
		'ms_ssh_ip', // string (IP address)
		'ms_ssh_port', // numeric
	);


    /**
     * Handle functions with problematic names or fail gracefully
     *
     */
	public function __call($method, $args) {
		if ($method == 'list')
		{
			return $this->listLinodes(isset($args[0]) ? $args[0] : 0);
		}
		elseif ($method == 'clone')
		{
            return $this->linodeClone(isset($args[0]) ? $args[0] : 0);
		}

		$className = get_class($this);

		throw new \BadMethodCallException("Call to undefined method {$className}::{$method}()");
	}

    //------------------------------------------------------------------------------
    //! Disk
    //------------------------------------------------------------------------------
	/**
	 * linode.disk.create
	 *
	 * https://www.linode.com/api/linode/linode.disk.create
	 *
	 * @param numeric $LinodeID
	 * @param string $Label
	 * @param string $Type The formatted type of this disk. Valid types are: ext3, swap, raw
	 * @param numeric $Size The size in MB of this Disk 
	 *
	 * @throws LinodeException
	 *
	 * @return number domain identifier
	 */
	public function diskCreate($LinodeID,$Label,$Type,$Size)
	{
        $options = array();
		$options['linodeid'] = (int) $LinodeID;
		$options['label'] = $Label;
		$options['type'] = $Type;
		$options['size'] = (int) $Size;
		$command = $this->prefix . '.disk.create';
		$request_headers = array();
		$request_options['query'] = $options;

		$data = $this->linode->get($command, $request_headers, $request_options);

		if (!array_key_exists('DiskID', $data)) throw new LinodeException("Invalid data returned from {$command} - no DiskID found");

		return $data['DiskID'];
	}   

	/**
	 * linode.disk.createfromdistribution
	 *
	 * https://www.linode.com/api/linode/linode.disk.createfromdistribution
	 *
	 * @param numeric $LinodeID
	 * @param numeric $DistributionID The DistributionID to create this disk from. Found in avail.distributions() 
	 * @param string $Label The label of this new disk image 
	 * @param numeric $Size The size in MB of this Disk 
	 * @param string $rootPass The root user's password 
	 * @param string $rootSSHKey Optionally sets this string into /root/.ssh/authorized_keys upon distribution configuration. 
	 *
	 * @throws LinodeException
	 *
	 * @return number domain identifier
	 */
	public function diskCreateFromDistribution($LinodeID,$DistributionID, $options) // ,$Label,$Size,$rootPass,$rootSSHKey=null)
	{
		$options['linodeid'] = (int) $LinodeID;
		$options['distributionid'] = (int) $DistributionID;
	/*
	$options['label'] = $Label;
		$options['size'] = (int) $Size;
		$options['rootpass'] = (int) $rootPass;
		if ($rootSSHKey) $options['rootsshkey'] = $rootSSHKey;
*/
		
		$command = $this->prefix . '.disk.createfromdistribution';
		$request_headers = array();
		$request_options['query'] = array_change_key_case(array_filter($options)); // $this->processOptions($options);

		$data = $this->linode->get($command, $request_headers, $request_options);

		if (!array_key_exists('DiskID', $data)) throw new LinodeException("Invalid data returned from {$command} - no DiskID found");

		return $data['DiskID'];
	}

	/**
	 * linode.disk.createfromstackscript
	 *
	 * https://www.linode.com/api/linode/linode.disk.createfromstackscript
	 *
	 * USAGE:
	 *
     *       $options = array();
     *       $options['StackScriptID'] = 1234;              // See the dashboard or avail.stackscripts
     *       $options['StackScriptUDFResponses'] = '{}';    // Must be valid JSON
     *       $options['DistributionID'] = 123;
     *       $options['Label'] = 'Main';
     *       $options['Size'] = 24320;
     *       $options['rootPass'] = 'my-root-password-here';
     *       
     *       $response = $linode->diskCreateFromStackScript(123456,$options);
	 *
	 * @param numeric $LinodeID
	 * @param array $options should contain the following keys:
	 *     StackScriptID numeric The StackScript to create this image from
	 *     StackScriptUDFResponses string JSON encoded name/value pairs, answering this StackScript's User Defined Fields 
	 *     DistributionID numeric Which Distribution to apply this StackScript to. Must be one from the script's DistributionIDList 
	 *     Label string The label of this new disk image
	 *     Size numeric Size of this disk image in MB
	 *     rootPass string The root user's password
	 *
	 * @throws LinodeException
	 *
	 * @return number disk identifier
	 */
	public function diskCreateFromStackScript($LinodeID, array $options)
	{
		$options['linodeid'] = (int) $LinodeID;
		$command = $this->prefix . '.disk.createfromstackscript';
		$request_headers = array();
		$request_options['query'] = $options;

		$data = $this->linode->get($command, $request_headers, $request_options);

		if (!array_key_exists('DiskID', $data)) throw new LinodeException("Invalid data returned from {$command} - no DiskID found");
		if (!array_key_exists('JobID', $data)) throw new LinodeException("Invalid data returned from {$command} - no JobID found");

		return $data['DiskID'];
	}  


	/**
	 * linode.disk.delete
	 *
	 * https://www.linode.com/api/linode/linode.disk.delete
	 *
	 * @param numeric $LinodeID
	 * @param numeric $DiskID (optional)
	 *
	 * @throws LinodeException
	 *
	 * @return number DiskID
	 */
	public function diskDelete($LinodeID, $DiskID)
	{
		$options['linodeid'] = (int) $LinodeID;
		if ($DiskID) $options['diskid'] = (int) $DiskID;
		$command = $this->prefix . '.disk.delete';
		$request_headers = array();
		$request_options['query'] = $options;

		$data = $this->linode->get($command, $request_headers, $request_options);

		if (!array_key_exists('DiskID', $data)) throw new LinodeException("Invalid data returned from {$command} - no DiskID found");
		if (!array_key_exists('JobID', $data)) throw new LinodeException("Invalid data returned from {$command} - no JobID found");
		
		return $data['DiskID'];
	} 

	/**
	 * linode.disk.list
	 *
	 * https://www.linode.com/api/linode/linode.disk.list
	 *
	 *
	 * @param numeric $LinodeID
	 * @param numeric $DiskID (optional) to return data for a specific disk
	 *
	 * @throws LinodeException
	 *
	 * @return array
	 */
	public function diskList($LinodeID, $DiskID=null)
	{
		$options['linodeid'] = (int) $LinodeID;
		if ($DiskID) $options['diskid'] = (int) $DiskID;
		$command = $this->prefix . '.disk.list';
		$request_headers = array();
		$request_options['query'] = $options;

		$data = $this->linode->get($command, $request_headers, $request_options);
		
		array_walk($data, function(&$item, $key) {
			$item = array_change_key_case($item);
		});

		return $data;
	} 

    //------------------------------------------------------------------------------
    //! Linode
    //------------------------------------------------------------------------------
	/**
	 * linode.boot
	 *
	 * Issues a boot job for the provided ConfigID. If no ConfigID is provided boots the last used configuration profile, 
	 * or the first configuration profile if this Linode has never been booted.
	 *
	 * @param array $options	array of key-value pairs for other optional values (see parameters array)
	 *
	 * @throws LinodeException
	 *
	 * @return number domain identifier
	 */
	public function boot($LinodeID, $ConfigID=null)
	{
		$options['linodeid'] = (int) $LinodeID;
        if ($ConfigID) $options['configid'] = (int) $ConfigID;
		$command = $this->prefix . '.boot';
		$request_headers = array();
		$request_options['query'] = $this->processOptions($options);

		$data = $this->linode->get($command, $request_headers, $request_options);

		if (!array_key_exists('JobID', $data)) throw new LinodeException("Invalid data returned from {$command} - no JobID found");

		return $data['JobID'];
	}

	/**
	 * linode.clone
	 *
	 * Issues a shutdown job for a given LinodeID.
	 *
	 * @param array $options	array of key-value pairs for other optional values (see parameters array)
	 *
	 * @throws LinodeException
	 *
	 * @return number domain identifier
	 */
	public function linodeClone($LinodeID, $ConfigID=null)
	{
		$options['linodeid'] = (int) $LinodeID;
        if ($ConfigID) $options['configid'] = (int) $ConfigID;
		$command = $this->prefix . '.boot';
		$request_headers = array();
		$request_options['query'] = $this->processOptions($options);

		$data = $this->linode->get($command, $request_headers, $request_options);

		if (!array_key_exists('JobID', $data)) throw new LinodeException("Invalid data returned from {$command} - no JobID found");

		return $data['JobID'];
	}

	/**
	 * linode.create
	 *
	 * Creates a Linode and assigns you full privileges. There is a 75-linodes-per-hour limiter.
	 *
	 * https://www.linode.com/api/linode/linode.create
	 *
     * @param numeric $DatacenterID The DatacenterID from avail.datacenters() where you wish to place this new Linode
     * @param numeric $PlanID The desired PlanID available from avail.LinodePlans() 
     * @param numeric $PaymentTerm Subscription term in months for non-metered customers. One of: 1, 12, or 24 (optional, default: 1)
	 *
	 * @throws LinodeException
	 *
	 * @return number domain identifier
	 */
	public function create($DatacenterID, $PlanID,$PaymentTerm=1)
	{
        $options = array();
		$options['datacenterid'] = $DatacenterID;
		$options['planid'] = $PlanID;
		$options['paymentterm'] = (int) $PaymentTerm;
		$command = $this->prefix . '.create';
		$request_headers = array();
		$request_options['query'] = $options;

		$data = $this->linode->get($command, $request_headers, $request_options);

		if (!array_key_exists('LinodeID', $data)) throw new LinodeException("Invalid data returned from {$command} - no LinodeID found");

		return $data['LinodeID'];
	}

	/**
	 * linode.delete
	 *
	 * https://www.linode.com/api/linode/linode.delete
	 *
	 * Warning: Linode must have no disks before delete!
	 *
	 * @param number $LinodeID	Linode to delete
	 * @param boolean $skipChecks Skips the safety checks and will always delete the Linode
	 * @throws LinodeException
	 * @return number LinodeID deleted
	 */
	public function delete($LinodeID,$skipChecks=false)
	{
        $options = array();
		$options['linodeid'] = (int) $LinodeID;
		$options['skipchecks'] = (bool) $skipChecks;
		$command = $this->prefix . '.delete';
		$request_headers = array();
		$request_options['query'] = $this->processOptions($options);

		$data = $this->linode->get($command, $request_headers, $request_options);

		if (!array_key_exists('LinodeID', $data)) throw new LinodeException("Invalid data returned from {$command} - no LinodeID found");

		return $data['LinodeID'];
	}

	/**
	 * linode.list
	 *
	 * @param number $linodeid	optional - if specified, lists details for this domain ID, otherwise lists all available domains
	 *
	 * @return array of LinodeData objects or single LinodeData object if linodeid specified
	 */

    public function listLinodes($LinodeID=null) {
		$command = $this->prefix . '.list';
		$request_headers = array();
		$request_options = array();

		$LinodeID = intval($LinodeID);

		if ($LinodeID > 0)
		{
			$request_options['query'] = array('LinodeID' => $LinodeID);
		}

		$response = $this->linode->get($command, $request_headers, $request_options);

		if (!is_array($response) OR empty($response))
		{
			return null;
		}

		if ($LinodeID > 0)
		{
			return array_change_key_case($response[0]);
		}

		array_walk($response, function(&$item, $key) {
			$item = array_change_key_case($item);
		});

		return $response;
	}

	/**
	 * linode.reboot
	 *
	 * https://www.linode.com/api/linode/linode.reboot
	 *	 
	 * @param number $LinodeID	linodeid to reboot
	 * @param number $ConfigID (optional)
	 *
	 * @throws LinodeException
	 *
	 * @return number job identifier
	 */
	public function reboot($LinodeID, $ConfigID = null)
	{
		$options['linodeid'] = (int) $LinodeID;
        if ($ConfigID) $options['configid'] = (int) $ConfigID;
        
		$command = $this->prefix . '.reboot';
		$request_headers = array();
		$request_options['query'] = $this->processOptions($options);

		$data = $this->linode->get($command, $request_headers, $request_options);

		if (!array_key_exists('JobID', $data)) throw new LinodeException("Invalid data returned from {$command} - no JobID found");

		return $data['JobID'];
	}

	/**
	 * linode.resize
	 *
	 * Resizes a Linode from one plan to another. Immediately shuts the Linode down, charges/credits the account, 
	 * and issue a migration to another host server.
	 *
	 * https://www.linode.com/api/linode/linode.resize
	 *
	 * @param number $LinodeID	linodeid to update
	 * @param number $PlanID (optional)
	 *
	 * @throws LinodeException
	 *
	 * @return number job identifier
	 */
	public function resize($LinodeID, $PlanID)
	{
        $options = array();
		$options['linodeid'] = (int) $LinodeID;
        $options['planid'] = (int) $PlanID;
        
		$command = $this->prefix . '.reboot';
		$request_headers = array();
		$request_options['query'] = $this->processOptions($options);

		$data = $this->linode->get($command, $request_headers, $request_options);

        // ???
		//if (!array_key_exists('JobID', $data)) throw new LinodeException("Invalid data returned from {$command} - no JobID found");

		return $data;
	}

	/**
	 * linode.shutdown
	 *
	 * https://www.linode.com/api/linode/linode.shutdown
	 *	 
	 * @param number $LinodeID	linodeid to reboot
	 *
	 * @throws LinodeException
	 *
	 * @return number job identifier
	 */
	public function shutdown($LinodeID)
	{
        $options = array();
		$options['linodeid'] = (int) $LinodeID;
        
		$command = $this->prefix . '.shutdown';
		$request_headers = array();
		$request_options['query'] = $this->processOptions($options);

		$data = $this->linode->get($command, $request_headers, $request_options);

		if (!array_key_exists('JobID', $data)) throw new LinodeException("Invalid data returned from {$command} - no JobID found");

		return $data['JobID'];
	}

	/**
	 * linode.update
	 *
	 * @param number $LinodeID	linodeid to update
	 * @param array $options	array of key-value pairs for other optional values (see defaults array)
	 *
	 * @throws LinodeException
	 *
	 * @return number linode identifier
	 */
	public function update($LinodeID, array $options = array())
	{
		$options['linodeid'] = $LinodeID;

		$command = $this->prefix . '.update';
		$request_headers = array();
		$request_options['query'] = $this->processOptions($options);

		$data = $this->linode->get($command, $request_headers, $request_options);

		if (!array_key_exists('LinodeID', $data)) throw new LinodeException("Invalid data returned from {$command} - no LinodeID found");

		return $data['LinodeID'];
	}
	
    //------------------------------------------------------------------------------
    //! Job
    //------------------------------------------------------------------------------
    /**
     * linode.job.list
     *
     * https://www.linode.com/api/linode/linode.job.list
     *
     * @param numeric $linodeid
     * @param numeric $jobid (optional)
     * @param boolean $pendingonly (optional: default false)
     */
    public function jobList($linodeid,$jobid=null,$pendingonly=false) {
		$options['linodeid'] = $linodeid;

        if ($jobid) $options['jobid'] = $jobid;
        if ($pendingonly) $options['pendingonly'] = $pendingonly;
        
		$command = $this->prefix . '.job.list';
		$request_headers = array();
		$request_options['query'] = $options;

		$data = $this->linode->get($command, $request_headers, $request_options);

        array_walk($data, function(&$item, $key) {
			$item = array_change_key_case($item);
		});
		
		return $data;
    
    }
    
    //------------------------------------------------------------------------------
    //! IP
    //------------------------------------------------------------------------------
    /**
     * linode.ip.list
     *
     * https://www.linode.com/api/linode/linode.ip.list
     *
     * @param numeric $linodeid 
     * @param number idaddressid (optional)
     */
    public function ipList($linodeid,$idaddressid=null) {
		$options['linodeid'] = $linodeid;

        if ($idaddressid) $options['idaddressid'] = $idaddressid;
        
		$command = $this->prefix . '.ip.list';
		$request_headers = array();
		$request_options['query'] = $options;
//throw new LinodeException("Invalid data returned from {$command} - no LinodeID found");
		$data = $this->linode->get($command, $request_headers, $request_options);

		return $data;
    
    }    

    //------------------------------------------------------------------------------
    //! Config
    //------------------------------------------------------------------------------
	/**
	 * linode.config.create
	 *
	 * Creates a Linode Configuration Profile
	 * https://www.linode.com/api/linode/linode.config.create
	 *
	 * @param number $LinodeID	optional - if specified, lists details for this domain ID, otherwise lists all available domains
	 * @param number $KernelID
	 * @param string $Label
	 * @param array options (optional) may contain any of the keys defined on https://www.linode.com/api/linode/linode.config.create
	 *
	 * @return number ConfigID
	 */
    public function configCreate($LinodeID,$KernelID,$Label,$options = array()) {
		$command = $this->prefix . '.config.create';
		$request_headers = array();
		$request_options = array();

		$options['linodeid'] = (int) $LinodeID;
		$options['kernelid'] = (int) $KernelID;
		$options['label'] = $Label;

        $request_options['query'] = $options;

		$data = $this->linode->get($command, $request_headers, $request_options);

        if (!array_key_exists('ConfigID', $data)) throw new LinodeException("Invalid data returned from {$command} - no ConfigID found");
        
        return $data['ConfigID'];
	}

	/**
	 * linode.config.delete
	 *
	 * Deletes a Linode Configuration Profile.
	 * https://www.linode.com/api/linode/linode.config.delete
	 *
	 * @param number $LinodeID
	 * @param number $ConfigID
	 *
	 * @return array
	 */

    public function configDelete($LinodeID,$ConfigID) {
		$command = $this->prefix . '.config.delete';
		$request_headers = array();
		$request_options = array();

        $options = array();
		$options['linodeid'] = (int) $LinodeID;
		$options['configid'] = (int) $ConfigID;

        $request_options['query'] = $options;

		$response = $this->linode->get($command, $request_headers, $request_options);

        return array_change_key_case($response);
	}

	/**
	 * linode.config.list
	 *
	 * Lists a Linode's Configuration Profiles.
	 * https://www.linode.com/api/linode/linode.config.list
	 *
	 * @param number $LinodeID	
	 * @param number $ConfigID (optional)
	 *
	 * @return array of Configuration Profiles
	 */

    public function configList($LinodeID,$ConfigID=null) {
		$command = $this->prefix . '.config.list';
		$request_headers = array();
		$request_options = array();

        $options = array();
		$options['linodeid'] = (int) $LinodeID;
		if (intval($ConfigID)) $options['configid'] = (int) $ConfigID;

        $request_options['query'] = $options;

		$response = $this->linode->get($command, $request_headers, $request_options);

        return array_change_key_case($response);
	}

	/**
	 * linode.config.update
	 *
	 * Updates a Linode Configuration Profile.
	 * https://www.linode.com/api/linode/linode.config.update
	 *
	 * @param number $LinodeID	
	 * @param number $ConfigID (optional)
	 *
	 * @return array of Configuration Profiles
	 */

    public function configUpdate($LinodeID,$ConfigID, $options=array()) {
		$command = $this->prefix . '.config.update';
		$request_headers = array();
		$request_options = array();

		$options['linodeid'] = (int) $LinodeID;
		$options['configid'] = (int) $ConfigID;

        $request_options['query'] = $options;

		$response = $this->linode->get($command, $request_headers, $request_options);

        return array_change_key_case($response);
	}

	
}
