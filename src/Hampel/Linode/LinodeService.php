<?php namespace Hampel\Linode;

use Guzzle\Http\Client;
use Hampel\Json\Json;

/**
 * The main service interface using Guzzle
 *
 */
class LinodeService
{
	/** @var string base url for API calls */
	protected $base_url = 'https://api.linode.com/';

	/** @var string api_key */
	protected $api_key;

	/** @var Client our Guzzle HTTP Client object */
	protected $client;

	/** @var Request Guzzle Request object representing the last request sent via Guzzle */
	protected $last_request;

	/** @var Response Guzzle Response object representing the last response from Guzzle call to Linode API */
	protected $last_response;

	/**
	 * Constructor
	 *
	 * @param Client $client		Guzzle HTTP client
	 * @param LinodeConfig $config	Config object
	 */
	public function __construct(Client $client, $api_key = "")
	{
		$this->client = $client;
		$this->api_key = $api_key;

		$this->client->setBaseUrl($this->base_url);
	}

	/**
	 * Factory - construct a service object
	 *
	 * @param string $api_key
	 * @return LinodeService a fully hydrated Linode Service, ready to run
	 */
	public static function factory($api_key)
	{
		return new static(new Client(), $api_key);
	}

	/**
	 * Make a GET call to the API via Guzzle
	 *
	 * @param string $command	Linode command to send
	 * @param array $request_headers	array of header information
	 * @param array $request_options	array of options, including API call parameters
	 *
	 * @throws LinodeException
	 *
	 * @return Response Guzzle Response object
	 */
	public function get($command, array $request_headers, array $request_options)
	{
		if (!isset($request_options['exceptions']))	$request_options['exceptions'] = false;

		$api_key = $this->getApiKey();

		if (!empty($api_key)) $request_options['query']['api_key'] = $api_key;
		$request_options['query']['api_action'] = $command;

		$request = $this->client->get("", $request_headers, $request_options);
		$this->last_request = $request;
		$response = $this->client->send($request);

		if (!$response->isSuccessful()) throw new LinodeException($response->getReasonPhrase(), $response->getStatusCode(), $response->getBody());
		$this->last_response = $response;

		$response_json = $response->getBody(true);
		if (empty($response_json)) throw new LinodeException("Empty body received from {$command}");

		$decoded_data = Json::decode($response_json, true);

		if (!array_key_exists('ERRORARRAY', $decoded_data)) throw new LinodeException("Invalid data received from {$command} - no ERRORARRAY");
		if (!array_key_exists('DATA', $decoded_data)) throw new LinodeException("Invalid data received from {$command} - no DATA");
		if (!empty($decoded_data['ERRORARRAY'])) throw new LinodeException("Error from Linode API call {$command}", null, $decoded_data['ERRORARRAY']);

		return $decoded_data['DATA'];
	}

	/**
	 * Return the status code from the last API call made
	 *
	 * @return number status code
	 */
	public function getLastStatusCode()
	{
		return $this->last_response->getStatusCode();
	}

	/**
	 * Return the request object from the last API call made
	 *
	 * @return Request Guzzle Request object
	 */
	public function getLastRequest()
	{
		return $this->last_request;
	}

	public function getLastQuery()
	{
		return $this->getLastRequest()->getQuery()->__toString();
	}

	/**
	 * Return the response object from the last API call made
	 *
	 * @return Response Guzzle Reponse object
	 */
	public function getLastResponse()
	{
		return $this->last_response;
	}

	public function setApiKey($api_key)
	{
		$this->api_key = $api_key;
	}

	public function getApiKey()
	{
		return $this->api_key;
	}

	/**
	 * request a new API Key from Linode using Linode manager username and password
	 *
	 * @param string $username	Linode manager usernmae
	 * @param string $password	Linode manager password
	 *
	 * @return string API key
	 */
	public static function requestApiKey($username, $password)
	{
		$linode = static::build(null);
		$user = new User($linode);

		return $user->getAPIKey($username, $password);
	}
}

?>