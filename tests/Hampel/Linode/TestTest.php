<?php namespace Hampel\Linode;

use Hampel\Linode\LinodeService;
use Hampel\Linode\Test;
use Guzzle\Service\Client as Client;
use Guzzle\Tests\GuzzleTestCase;

class TestTest extends GuzzleTestCase
{
	public function setUp()
	{
		$this->setMockBasePath(dirname(__FILE__) . DIRECTORY_SEPARATOR . "mock");
	}

	public function testEcho()
	{
		$client = new Client();
		$this->setMockResponse($client, 'test_echo.json');

		$linode = new LinodeService($client);
		$test = new Test($linode);
		$response = $test->echo(array("foo" => "bar"));

		$this->assertEquals(200, $linode->getLastStatusCode());
		$this->assertArrayHasKey('foo', $response);
		$this->assertEquals('bar', $response['foo']);
	}
}

?>