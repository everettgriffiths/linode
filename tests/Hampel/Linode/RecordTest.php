<?php namespace Hampel\Linode;

use Hampel\Linode\LinodeService;
use Guzzle\Service\Client;
use Guzzle\Tests\GuzzleTestCase;

class RecordTest extends GuzzleTestCase
{
	public function setUp()
	{
		$this->setMockBasePath(dirname(__FILE__) . DIRECTORY_SEPARATOR . "mock");
	}

	public function testCreate()
	{
		$client = new Client();
		$this->setMockResponse($client, 'domain_resource_create.json');

		$linode = new LinodeService($client);
		$record = new Record($linode);
		$response = $record->create(12345, "A", array("target" => "10.0.0.1"));

		$this->assertEquals('domainid=12345&type=A&target=10.0.0.1&api_action=domain.resource.create', $linode->getLastQuery());
		$this->assertEquals(200, $linode->getLastStatusCode());
		$this->assertEquals(54321, $response);
	}

	public function testCreateMx()
	{
		$client = new Client();
		$this->setMockResponse($client, 'domain_resource_create.json');

		$linode = new LinodeService($client);
		$record = new Record($linode);
		$response = $record->create(12345, 'MX', array('target' => 'mail.mock-domain.com'));

		$this->assertEquals('domainid=12345&type=MX&target=mail.mock-domain.com&api_action=domain.resource.create', $linode->getLastQuery());
		$this->assertEquals(200, $linode->getLastStatusCode());
		$this->assertEquals(54321, $response);
	}

	public function testCreateA()
	{
		$client = new Client();
		$this->setMockResponse($client, 'domain_resource_create.json');

		$linode = new LinodeService($client);
		$record = new Record($linode);
		$response = $record->create(12345, 'A', array('name' => "mock-domain.com", 'target' => "10.0.0.1"));

		$this->assertEquals('domainid=12345&type=A&name=mock-domain.com&target=10.0.0.1&api_action=domain.resource.create', $linode->getLastQuery());
		$this->assertEquals(200, $linode->getLastStatusCode());
		$this->assertEquals(54321, $response);
	}

	public function testCreateCname()
	{
		$client = new Client();
		$this->setMockResponse($client, 'domain_resource_create.json');

		$linode = new LinodeService($client);
		$record = new Record($linode);
		$response = $record->create(12345, 'CNAME', array('name' => "mock", 'target' => "mock-domain.com"));

		$this->assertEquals('domainid=12345&type=CNAME&name=mock&target=mock-domain.com&api_action=domain.resource.create', $linode->getLastQuery());
		$this->assertEquals(200, $linode->getLastStatusCode());
		$this->assertEquals(54321, $response);
	}

	public function testUpdate()
	{
		$client = new Client();
		$this->setMockResponse($client, 'domain_resource_update.json');

		$linode = new LinodeService($client);
		$record = new Record($linode);
		$response = $record->update(12345, 54321, array("target" => "10.0.0.1"));

		$this->assertEquals('domainid=12345&resourceid=54321&target=10.0.0.1&api_action=domain.resource.update', $linode->getLastQuery());
		$this->assertEquals(200, $linode->getLastStatusCode());
		$this->assertEquals(54321, $response);
	}

	public function testUpdateMx()
	{
		$client = new Client();
		$this->setMockResponse($client, 'domain_resource_update.json');

		$linode = new LinodeService($client);
		$record = new Record($linode);
		$response = $record->update(12345, 54321, array('target' => "mail.mock-domain.com"));

		$this->assertEquals('domainid=12345&resourceid=54321&target=mail.mock-domain.com&api_action=domain.resource.update', $linode->getLastQuery());
		$this->assertEquals(200, $linode->getLastStatusCode());
		$this->assertEquals(54321, $response);
	}

	public function testUpdateA()
	{
		$client = new Client();
		$this->setMockResponse($client, 'domain_resource_update.json');

		$linode = new LinodeService($client);
		$record = new Record($linode);
		$response = $record->update(12345, 54321, array('name' => 'mail', 'target' => '10.0.0.1'));

		$this->assertEquals('domainid=12345&resourceid=54321&name=mail&target=10.0.0.1&api_action=domain.resource.update', $linode->getLastQuery());
		$this->assertEquals(200, $linode->getLastStatusCode());
		$this->assertEquals(54321, $response);
	}

	public function testUpdateCname()
	{
		$client = new Client();
		$this->setMockResponse($client, 'domain_resource_update.json');

		$linode = new LinodeService($client);
		
		$record = new Record($linode);
		$response = $record->update(12345, 54321, array('name' => "mail", 'target' => "mail.mock-domain.com"));

		$this->assertEquals('domainid=12345&resourceid=54321&name=mail&target=mail.mock-domain.com&api_action=domain.resource.update', $linode->getLastQuery());
		$this->assertEquals(200, $linode->getLastStatusCode());
		$this->assertEquals(54321, $response);
	}

	public function testDelete()
	{
		$client = new Client();
		$this->setMockResponse($client, 'domain_resource_delete.json');

		$linode = new LinodeService($client);
		
		$record = new Record($linode);
		$response = $record->delete(12345, 54321);

		$this->assertEquals('domainid=12345&resourceid=54321&api_action=domain.resource.delete', $linode->getLastQuery());
		$this->assertEquals(200, $linode->getLastStatusCode());
		$this->assertEquals(54321, $response);
	}

	public function testListSingle()
	{
		$client = new Client();
		$this->setMockResponse($client, 'domain_resource_list_single.json');

		$linode = new LinodeService($client);
		
		$record = new Record($linode);
		$response = $record->list(12345, 54321);

		$this->assertEquals('domainid=12345&resourceid=54321&api_action=domain.resource.list', $linode->getLastQuery());
		$this->assertEquals(200, $linode->getLastStatusCode());
		$this->assertEquals(54321, $response['resourceid']);
		$this->assertEquals('A', $response['type']);
	}

	public function testList()
	{
		$client = new Client();
		$this->setMockResponse($client, 'domain_resource_list_multiple.json');

		$linode = new LinodeService($client);
		
		$record = new Record($linode);
		$response = $record->list(12345);

		$this->assertEquals('domainid=12345&api_action=domain.resource.list', $linode->getLastQuery());
		$this->assertEquals(200, $linode->getLastStatusCode());
		$this->assertEquals(2, count($response));
		$this->assertArrayHasKey(0, $response);
		$this->assertEquals(54321, $response[0]['resourceid']);
		$this->assertEquals('A', $response[0]['type']);
		$this->assertArrayHasKey(1, $response);
		$this->assertEquals(54322, $response[1]['resourceid']);
		$this->assertEquals('A', $response[1]['type']);
	}
}

?>