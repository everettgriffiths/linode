<?php namespace Hampel\Linode;

use Hampel\Linode\LinodeService;
use Hampel\Linode\User;
use Guzzle\Service\Client;
use Guzzle\Tests\GuzzleTestCase;

class UserTest extends GuzzleTestCase
{
	public function setUp()
	{
		$this->setMockBasePath(dirname(__FILE__) . DIRECTORY_SEPARATOR . "mock");
	}

	public function testGetAPIKey()
	{
		$client = new Client();
		$this->setMockResponse($client, 'user_getapikey.json');

		$linode = new LinodeService($client, null);
		$user = new User($linode);
		$response = $user->getAPIKey("mock_username", "mock_password");

		$this->assertEquals(200, $linode->getLastStatusCode());
		$this->assertEquals("mock_api_key", $response);
	}
}

?>