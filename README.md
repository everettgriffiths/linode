Hampel Linode
=============

A Linode API wrapper using Guzzle

By [Simon Hampel](http://hampelgroup.com/).

Installation
------------

The recommended way of installing Hampel Linode is through [Composer](http://getcomposer.org):

    {
        "require": {
            "hampel/linode": "2.0.*"
        }
    }
    
Usage
-----

*Note: v2.0 of this wrapper is not backwards compatible with v1.0.x - applications using this wrapper will need to be re-written to utilise
this new interface. However, the interface is now substantially simplified, with data returned using associative arrays rather than objects*

    <?php

	use Hampel\Linode\LinodeService;
	use Hampel\Linode\Test;

	// long-hand initialisation method
	$client = new Guzzle\Http\Client();
	$linode = new LinodeService($client, 'api_key');

	// alternative static factory
	$linode = LinodeService::factory("your-api-key-here-xxxxxxxxxxxx");
		
	// echo test, API echoes back parameters passed
	$test = new Test($linode);
	$response = $test->echoTest(array("foo" => "bar"));
	
	var_dump($response);
	
	// get api key
	$user = new User($linode);
	$response = $user->getAPIKey("username", "password");

	var_dump($response);

	// alternative static method for getting API key without needing to instantiate LinodeService
	$response = LinodeService::requestAPIKey("username", "password");

	var_dump($response);

	// create a new domain entry
	$domain = new Domain($linode);
	$options = array("soa_email" => "soa_email@domain-name.com", "ttl_sec" => 3600);
	$response = $domain->create("domain-name.com", "master", $options);

	var_dump($response);
	
	// retrieve details about a domain
	$domain = new Domain($linode);
	$response = $domain->list(12345); // specify domain id as parameter
	
	
	?>


Creating a Linode Programmatically
----------------------------------

Spinning up a working server requires several steps and relies on several API methods.  The process goes as follows:

1. Create the server instance via linode.create (it's a bit like an empty shell at this point)
2. Create the primary disk on the server e.g. via linode.disk.createfromdistribution or linode.disk.createfromstackscript
3. Create the swap disk on the server e.g. via linode.disk.create
4. Create the configuration profile for the server via linode.config.create
5. Boot the server via linode.boot

In practice, this looks something like the following:


    <?php
    $linodeservice = LinodeService::factory("your-api-key-here-xxxxxxxxxxxx");
    $linode = new Linode($linodeservice);
    try {
        $LinodeID = $linode->create(2,1,1);            
        $DiskID = $linode->diskCreateFromStackScript($LinodeID,
            array(
                'StackScriptID' => 1234,
                'StackScriptUDFResponses' => '{}',
                'DistributionID' => 123,
                'Label' => 'Primary',
                'Size' => 24320,
                'rootPass' => 'your-root-password-here'
            )
        );
        $SwapDiskID = $linode->diskCreate($LinodeID,'Swap','swap','256');
        $ConfigID = $linode->configCreate(498273,138,'Standard Profile',
            array(
                'DiskList' => "$DiskID,$SwapDiskID,,,,,,,",
                'RootDeviceNum' => 1
            )
        );
        $JobID = $linode->boot($LinodeID);
    }
    catch (Exception $e) {
        return $e->getMessage();
    }
    ?>


Deleting a Linode Programmatically
----------------------------------

Deleting a Linode isn't as simple as issuing the delete command.  One "gotcha" is that the server must be powered off.

1. Shutdown the server.
2. Delete the configuration profile.
3. Delete the disks.
4. Delete the server.

    <?php
    $linodeservice = LinodeService::factory("your-api-key-here-xxxxxxxxxxxx");
    $linode = new Linode($linodeservice);
    $LinodeID = 'your-linode-id';
    $linode->shutdown($LinodeID);
    
    $diskdata = $linode->diskList($LinodeID);
    foreach ($diskdata as $d) {
        $linode->diskDelete($LinodeID,$d['diskid']);
    }
    $linode->delete($LinodeID,true);
    
    ?>

Notes
-----
 
The following calls have been implemented:

* domain.*
* domain.resource.*
* test.*
* user.*
 
TODO: 

The following calls still need to be implemented:

* linode.*
* linode.config.*
* linode.disk.*
* linode.ip.*
* linode.job.*
* nodebalancer.*
* nodebalancer.config.*
* nodebalancer.node.*
* stackscript.*
* api.*
* account.*
* avail.*

Unit Testing
------------

Rename phpunit.xml.dist to phpunit.xml to set up unit testing, configure your API Key in the php section:

	<php>
		<const name="API_KEY" value="Linode API Key goes here" />
	</php>
	
To run mock tests only and ignore network tests, run: phpunit --exclude-group network
